from tkinter import *
from tkinter import ttk  # Для работы со вкладками
from tkinter import messagebox
import cmath
import numpy as np

debug = True


def mk(a):
    try:
        if a == "":
            return 0
        if '.' in a or ',' in a:
            return float(a)
        else:
            return int(a)
    except:
        return 0


def solve_eq(a, b, c):
    try:
        if a == 0:
            if b == 0:
                return "Fail"
            else:
                return -c / b
        else:
            discr = b ** 2 - 4 * a * c
            x1 = (-b - cmath.sqrt(discr)) / (2 * a)
            x2 = (-b + cmath.sqrt(discr)) / (2 * a)

            x1_real = round(float(x1.real), 2)
            x1_imag = round(float(x1.imag), 2)
            x2_real = round(float(x2.real), 2)
            x2_imag = round(float(x2.imag), 2)
            if x1_imag != 0 or x2_imag != 0:
                res = [f"x1 = {x1_real}+{x1_imag}j",
                       f"x2 = {x2_real}+{x2_imag}j"]
            else:
                res = [f"x1 = {x1_real}\n",
                       f"x2 = {x2_real}\n"]
            return res
        return "Fail"
    except OverflowError:
        return "Fail"


def solveSLAU4():
    try:
        matrix = np.zeros((4, 4))
        b_matrix = np.zeros((4, 1))
        matrix[0][0] = mk(x_text00_tab3.get())
        matrix[1][0] = mk(x_text10_tab3.get())
        matrix[2][0] = mk(x_text20_tab3.get())
        matrix[3][0] = mk(x_text30_tab3.get())

        matrix[0][1] = mk(y_text01_tab3.get())
        matrix[1][1] = mk(y_text11_tab3.get())
        matrix[2][1] = mk(y_text21_tab3.get())
        matrix[3][1] = mk(y_text31_tab3.get())

        matrix[0][2] = mk(z_text02_tab3.get())
        matrix[1][2] = mk(z_text12_tab3.get())
        matrix[2][2] = mk(z_text22_tab3.get())
        matrix[3][2] = mk(z_text32_tab3.get())

        matrix[0][3] = mk(t_text03_tab3.get())
        matrix[1][3] = mk(t_text13_tab3.get())
        matrix[2][3] = mk(t_text23_tab3.get())
        matrix[3][3] = mk(t_text33_tab3.get())

        b_matrix[0][0] = mk(b_text05_tab3.get())
        b_matrix[1][0] = mk(b_text15_tab3.get())
        b_matrix[2][0] = mk(b_text25_tab3.get())
        b_matrix[3][0] = mk(b_text35_tab3.get())

        list_solves = np.linalg.solve(matrix, b_matrix)
        list_solves = [round(float(i), 2) for i in list_solves]

        lbl_answerX_tab3.configure(text=f"x = {list_solves[0]}")
        lbl_answerY_tab3.configure(text=f"y = {list_solves[1]}")
        lbl_answerZ_tab3.configure(text=f"z = {list_solves[2]}")
        lbl_answerT_tab3.configure(text=f"t = {list_solves[3]}")

    except:
        messagebox.showerror('Fail', 'Fail')


def solveSLAU3():
    try:
        matrix = np.zeros((3, 3))
        b_matrix = np.zeros((3, 1))
        matrix[0][0] = mk(x_text00.get())
        matrix[1][0] = mk(x_text10.get())
        matrix[2][0] = mk(x_text20.get())

        matrix[0][1] = mk(y_text00.get())
        matrix[1][1] = mk(y_text10.get())
        matrix[2][1] = mk(y_text20.get())

        matrix[0][2] = mk(z_text00.get())
        matrix[1][2] = mk(z_text10.get())
        matrix[2][2] = mk(z_text20.get())

        b_matrix[0][0] = mk(b_text00.get())
        b_matrix[1][0] = mk(b_text10.get())
        b_matrix[2][0] = mk(b_text20.get())
    except ValueError:
        messagebox.showerror('Опасность!', 'Вы буквы хотите решить?')
    try:
        list_solves = np.linalg.solve(matrix, b_matrix)
        list_solves = [round(float(i), 2) for i in list_solves]
        lbl_answerX_tab2.configure(text=f"x = {list_solves[0]}")
        lbl_answerY_tab2.configure(text=f"y = {list_solves[1]}")
        lbl_answerZ_tab2.configure(text=f"z = {list_solves[2]}")
    except:
        messagebox.showerror('Fail', "Fail")


def clickTab1():
    a = mk(coefficient_a.get())
    b = mk(coefficient_b.get())
    c = mk(coefficient_c.get())
    if debug: print(f"a={a}\nb={b}\nc={c}")
    res = solve_eq(a, b, c)
    if res == "Fail":
        messagebox.showerror("Что-то пошло не так", "Fail")
    else:
        lblAnswX1.configure(text=res[0])
        lblAnswX2.configure(text=res[1])


mainWindow = Tk()

# Делаю окно фиксированного размера
mainWindow.minsize(400, 350)  # Минимальный размер окна
mainWindow.maxsize(400, 350)  # Максимальный размер окна
# _________________________________

# Изменяю имя программы и картинку логотипа
mainWindow.title("myMath")  # Имя программы
mainWindow.iconbitmap("./Look/image.ico")  # Расширение картинки ico
# __________________________________

# Создаю и подписываю рабочие вкладки
tab_control = ttk.Notebook(mainWindow)  # Сюда будут добавляться вкладки

tab1 = ttk.Frame(tab_control)  # Создание объектов вкладок
tab2 = ttk.Frame(tab_control)
tab3 = ttk.Frame(tab_control)

tab_control.add(tab3, text="  СЛАУ 4-го порядка   ")
tab_control.add(tab2, text="  СЛАУ 3-го порядка   ")
tab_control.add(tab1, text="Квадратное уравнение   ")
# Пробелы, чтобы вкладки были во всю ширину
# ____________________________________


# Работа с первой вкладкой (Квадратное уравнение)

font = ("libertine", 12)  # аргумент, описывающий шрифт и размер текста

# Создание и отрисовка текстовых полей
coefficient_a = Entry(tab1, width=3, font=font, justify=RIGHT)
coefficient_b = Entry(tab1, width=3, font=font, justify=RIGHT)
coefficient_c = Entry(tab1, width=3, font=font, justify=RIGHT)
coefficient_a.focus()  # Без лишнего клика сразу можно вводить текст с клавиатуры
# В методе grid column - столбцы; row - строки
coefficient_a.grid(column=1, row=1)
coefficient_b.grid(column=4, row=1)
coefficient_c.grid(column=6, row=1)

# Создание и отрисовка меток
NothingLbl1 = Label(tab1, text="").grid(column=0, row=0)  # Для сдвига уравений на одну строку
lblBefX2 = Label(tab1, text="(", font=font).grid(column=0, row=1, sticky=E)
lblX2 = Label(tab1, text=") x^2", font=font).grid(column=2, row=1)
lblBefX = Label(tab1, text="+ (", font=font).grid(column=3, row=1)
lblX = Label(tab1, text=") x + (", font=font).grid(column=5, row=1)
lblBefC = Label(tab1, text=") = 0", font=font).grid(column=7, row=1)
NothingLbl2 = Label(tab1, text="").grid(column=0, row=2)  # Для сдвига корней на одну строку

lblAnswX1 = Label(tab1, text="x1 = ", font=font)
lblAnswX1.grid(columnspan=14, row=2, sticky=W)
lblAnswX2 = Label(tab1, text="x2 = ", font=font)
lblAnswX2.grid(columnspan=14, row=3, sticky=W)

btnSolveIt = Button(tab1, text="Решить", font=font, command=clickTab1)
btnSolveIt.grid(column=8, row=1, sticky=W)  # кнопка для запуска решения

# Конец работы с первой вкладкой _____________

# Работа со второй вкладкой
NothingLbl_tab2 = Label(tab2, text='')
NothingLbl_tab2.grid(column=0, row=0)

lblTab2_00 = Label(tab2, text="( ", font=font)
lblTab2_10 = Label(tab2, text="( ", font=font)
lblTab2_20 = Label(tab2, text="( ", font=font)
lblTab2_00.grid(column=0, row=1)
lblTab2_10.grid(column=0, row=2)
lblTab2_20.grid(column=0, row=3)

x_text00 = Entry(tab2, width=3, font=font, justify=RIGHT)
x_text10 = Entry(tab2, width=3, font=font, justify=RIGHT)
x_text20 = Entry(tab2, width=3, font=font, justify=RIGHT)
x_text00.grid(column=1, row=1)
x_text10.grid(column=1, row=2)
x_text20.grid(column=1, row=3)

lblTab2_01 = Label(tab2, text=" ) x + ( ", font=font)
lblTab2_11 = Label(tab2, text=" ) x + ( ", font=font)
lblTab2_21 = Label(tab2, text=" ) x + ( ", font=font)
lblTab2_01.grid(column=2, row=1)
lblTab2_11.grid(column=2, row=2)
lblTab2_21.grid(column=2, row=3)

y_text00 = Entry(tab2, width=3, font=font, justify=RIGHT)
y_text10 = Entry(tab2, width=3, font=font, justify=RIGHT)
y_text20 = Entry(tab2, width=3, font=font, justify=RIGHT)
y_text00.grid(column=3, row=1)
y_text10.grid(column=3, row=2)
y_text20.grid(column=3, row=3)

lblTab2_02 = Label(tab2, text=" ) y + ( ", font=font)
lblTab2_12 = Label(tab2, text=" ) y + ( ", font=font)
lblTab2_22 = Label(tab2, text=" ) y + ( ", font=font)
lblTab2_02.grid(column=4, row=1)
lblTab2_12.grid(column=4, row=2)
lblTab2_22.grid(column=4, row=3)

z_text00 = Entry(tab2, width=3, font=font, justify=RIGHT)
z_text10 = Entry(tab2, width=3, font=font, justify=RIGHT)
z_text20 = Entry(tab2, width=3, font=font, justify=RIGHT)
z_text00.grid(column=5, row=1)
z_text10.grid(column=5, row=2)
z_text20.grid(column=5, row=3)

lblTab2_03 = Label(tab2, text=" ) z = ( ", font=font)
lblTab2_13 = Label(tab2, text=" ) z = ( ", font=font)
lblTab2_23 = Label(tab2, text=" ) z = ( ", font=font)
lblTab2_03.grid(column=6, row=1)
lblTab2_13.grid(column=6, row=2)
lblTab2_23.grid(column=6, row=3)

b_text00 = Entry(tab2, width=3, font=font, justify=RIGHT)
b_text10 = Entry(tab2, width=3, font=font, justify=RIGHT)
b_text20 = Entry(tab2, width=3, font=font, justify=RIGHT)
b_text00.grid(column=7, row=1)
b_text10.grid(column=7, row=2)
b_text20.grid(column=7, row=3)

lblTab2_00 = Label(tab2, text=")", font=font)
lblTab2_10 = Label(tab2, text=")", font=font)
lblTab2_20 = Label(tab2, text=")", font=font)
lblTab2_00.grid(column=8, row=1)
lblTab2_10.grid(column=8, row=2)
lblTab2_20.grid(column=8, row=3)

lbl_answerX_tab2 = Label(tab2, text="x = ", font=font)
lbl_answerY_tab2 = Label(tab2, text="y = ", font=font)
lbl_answerZ_tab2 = Label(tab2, text="z = ", font=font)
NothingLbl1_tab2 = Label
lbl_answerX_tab2.grid(columnspan=10, row=6, sticky=W)
lbl_answerY_tab2.grid(columnspan=10, row=7, sticky=W)
lbl_answerZ_tab2.grid(columnspan=10, row=8, sticky=W)

btnSolveit_tab2 = Button(tab2, text="Найти корни", font=font, command=solveSLAU3)
btnSolveit_tab2.grid(columnspan=10, row=5)

# Конец работы со второй вкладкой ____________________

# Работа с третьей вкладкой __________________________
nothingLbl_tab3 = Label(tab3, text="").grid(column=0, row=0)

lbl_tab3_00 = Label(tab3, text="( ", font=font)
lbl_tab3_10 = Label(tab3, text="( ", font=font)
lbl_tab3_20 = Label(tab3, text="( ", font=font)
lbl_tab3_30 = Label(tab3, text="( ", font=font)
lbl_tab3_00.grid(column=0, row=1)
lbl_tab3_10.grid(column=0, row=2)
lbl_tab3_20.grid(column=0, row=3)
lbl_tab3_30.grid(column=0, row=4)

x_text00_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
x_text10_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
x_text20_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
x_text30_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
x_text00_tab3.grid(column=1, row=1)
x_text10_tab3.grid(column=1, row=2)
x_text20_tab3.grid(column=1, row=3)
x_text30_tab3.grid(column=1, row=4)

lbl_tab3_01 = Label(tab3, text=" ) x + ( ", font=font)
lbl_tab3_11 = Label(tab3, text=" ) x + ( ", font=font)
lbl_tab3_21 = Label(tab3, text=" ) x + ( ", font=font)
lbl_tab3_31 = Label(tab3, text=" ) x + ( ", font=font)
lbl_tab3_01.grid(column=2, row=1)
lbl_tab3_11.grid(column=2, row=2)
lbl_tab3_21.grid(column=2, row=3)
lbl_tab3_31.grid(column=2, row=4)

y_text01_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
y_text11_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
y_text21_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
y_text31_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
y_text01_tab3.grid(column=3, row=1)
y_text11_tab3.grid(column=3, row=2)
y_text21_tab3.grid(column=3, row=3)
y_text31_tab3.grid(column=3, row=4)

lbl_tab3_02 = Label(tab3, text=" ) y + ( ", font=font)
lbl_tab3_12 = Label(tab3, text=" ) y + ( ", font=font)
lbl_tab3_22 = Label(tab3, text=" ) y + ( ", font=font)
lbl_tab3_32 = Label(tab3, text=" ) y + ( ", font=font)
lbl_tab3_02.grid(column=4, row=1)
lbl_tab3_12.grid(column=4, row=2)
lbl_tab3_22.grid(column=4, row=3)
lbl_tab3_32.grid(column=4, row=4)

z_text02_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
z_text12_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
z_text22_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
z_text32_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
z_text02_tab3.grid(column=5, row=1)
z_text12_tab3.grid(column=5, row=2)
z_text22_tab3.grid(column=5, row=3)
z_text32_tab3.grid(column=5, row=4)

lbl_tab3_03 = Label(tab3, text=" ) z + ( ", font=font)
lbl_tab3_13 = Label(tab3, text=" ) z + ( ", font=font)
lbl_tab3_23 = Label(tab3, text=" ) z + ( ", font=font)
lbl_tab3_33 = Label(tab3, text=" ) z + ( ", font=font)
lbl_tab3_03.grid(column=6, row=1)
lbl_tab3_13.grid(column=6, row=2)
lbl_tab3_23.grid(column=6, row=3)
lbl_tab3_33.grid(column=6, row=4)

t_text03_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
t_text13_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
t_text23_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
t_text33_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
t_text03_tab3.grid(column=7, row=1)
t_text13_tab3.grid(column=7, row=2)
t_text23_tab3.grid(column=7, row=3)
t_text33_tab3.grid(column=7, row=4)

lbl_tab3_04 = Label(tab3, text=" ) t = ( ", font=font)
lbl_tab3_14 = Label(tab3, text=" ) t = ( ", font=font)
lbl_tab3_24 = Label(tab3, text=" ) t = ( ", font=font)
lbl_tab3_34 = Label(tab3, text=" ) t = ( ", font=font)
lbl_tab3_04.grid(column=8, row=1)
lbl_tab3_14.grid(column=8, row=2)
lbl_tab3_24.grid(column=8, row=3)
lbl_tab3_34.grid(column=8, row=4)

b_text05_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
b_text15_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
b_text25_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
b_text35_tab3 = Entry(tab3, width=3, font=font, justify=RIGHT)
b_text05_tab3.grid(column=9, row=1)
b_text15_tab3.grid(column=9, row=2)
b_text25_tab3.grid(column=9, row=3)
b_text35_tab3.grid(column=9, row=4)

lbl_tab3_05 = Label(tab3, text=" )", font=font)
lbl_tab3_15 = Label(tab3, text=" )", font=font)
lbl_tab3_25 = Label(tab3, text=" )", font=font)
lbl_tab3_35 = Label(tab3, text=" )", font=font)
lbl_tab3_05.grid(column=10, row=1)
lbl_tab3_15.grid(column=10, row=2)
lbl_tab3_25.grid(column=10, row=3)
lbl_tab3_35.grid(column=10, row=4)

lbl_answerX_tab3 = Label(tab3, text="x = ", font=font)
lbl_answerY_tab3 = Label(tab3, text="y = ", font=font)
lbl_answerZ_tab3 = Label(tab3, text="z = ", font=font)
lbl_answerT_tab3 = Label(tab3, text="t = ", font=font)
lbl_answerX_tab3.grid(columnspan=11, row=6, sticky=W)
lbl_answerY_tab3.grid(columnspan=11, row=7, sticky=W)
lbl_answerZ_tab3.grid(columnspan=11, row=8, sticky=W)
lbl_answerT_tab3.grid(columnspan=11, row=9, sticky=W)

btnSolveIt_tab3 = Button(tab3, text="Найти корни", font=font, command=solveSLAU4)
btnSolveIt_tab3.grid(columnspan=10, row=5)

tab_control.pack(expand=1, fill='both')  # Вывод вкладок на главное окно
mainWindow.mainloop()  # Запуск программы
